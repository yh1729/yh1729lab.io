# home
- [home](https://yh1729.gitlab.io/index.html)
- [about](https://yh1729.gitlab.io/about.html)
- notes
  - [differential equations](https://yh1729.gitlab.io/differential_equations.html)
  - [stochastic analysis](https://yh1729.gitlab.io/stochastic_analysis.html)
- posts
  - [paperization](https://yh1729.gitlab.io/paperization.html)
  - [an introduction to Chebyshev polynomials](https://yh1729.gitlab.io/an_introduction_to_Chebyshev_polynomials.html)
